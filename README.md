# README #

### What is this repository for? ###

It shows how to use ORBM2MKit.framework

### How do I get set up? ###

* "bitcode" must set to 'NO'
* "Add ORBM2MKit.framework into 'Embedded Binaries'"
    ![螢幕快照 2016-06-17 下午1.54.12.png](https://bitbucket.org/repo/K7aLdM/images/946080131-%E8%9E%A2%E5%B9%95%E5%BF%AB%E7%85%A7%202016-06-17%20%E4%B8%8B%E5%8D%881.54.12.png)

### Import ORBM2MKit.framework
```objective-c
#import <OrbwebM2MKit/OrbwebM2MKit.h>
```

## Architecture

### OrbwebM2MKit

- `ORBM2MConnectionManager`
- `ORBM2MSocketManager`

## Usage

### ORBM2MConnectionManager

`ORBM2MConnectionManager` creates and manages an `ORBM2MConnection` object 

#### Creating a Connection Task
###### OBJECTIVE-C
```objective-c
- (void)creatTunnelConnection {
    NSString *SID = @"";/**Input your SID**/
    [ORBM2MConnectionManager defaultManager].mDelegate = self;
    [[ORBM2MConnectionManager defaultManager]connectWithSID:SID success:^(ORBM2MConnection *connection, NSError* error){
            if (connection != nil) {
                NSLog(@"rtspMappedPort: %ld", (long)connection.rtspMappedPort);
                NSLog(@"socketMappedPort: %ld", (long)connection.socketMappedPort);
                NSLog(@"httpMappedPort: %ld", (long)connection.httpMappedPort);
            }
            else{
                NSLog(@"Error: %@", error.localizedDescription);
            }
        } renew:^(ORBM2MConnection *connection, NSError *error){
            if (connection != nil) {
                NSLog(@"renew rtspMappedPort: %ld", (long)connection.rtspMappedPort);
                NSLog(@"renew socketMappedPort: %ld", (long)connection.socketMappedPort);
                NSLog(@"renew httpMappedPort: %ld", (long)connection.httpMappedPort);
            }
            else{
                NSLog(@"Error: %@", error.localizedDescription);
            }
        }];
}

//MARK: - ORBM2MConnectionManagerDelegate (must required)
-(NSInteger)connectionSocketPort{
    return KIPCamSocketPort;
}

-(NSInteger)connectionHttpPort{
    return KIPCamHttpPort;
}

```
###### SWIFT
```swift
    func createTunnelConnection -> Void {
        let SID = ""/**Input your SID**/
        ORBM2MConnectionManager.defaultManager().mDelegate = self
        ORBM2MConnectionManager.defaultManager().connectWithSID(SID, success: {
                connection, error -> Void in
                if connection != nil {
                    print("socketMappedPort: \(connection!.socketMappedPort)")
                    print("rtspMappedPort: \(connection!.rtspMappedPort)")
                    print("httpMappedPort: \(connection!.httpMappedPort)")
                    }
                
                }
                else{
                    print("ERROR[\(error!.code)]\nDomain: \(error!.domain)\nDescription:\(error!.localizedDescription)")
            }
        }, renew: {
                connection, error -> Void in
                if connection != nil {
                    print("renew socketMappedPort: \(connection!.socketMappedPort)")
                    print("renew rtspMappedPort: \(connection!.rtspMappedPort)")
                    print("renew httpMappedPort: \(connection!.httpMappedPort)")
                }
                else{
                    print("ERROR[\(error!.code)]\nDomain: \(error!.domain)\nDescription:\(error!.localizedDescription)")
                }
        })
    }

  //MARK: - ORBM2MConnectionManagerDelegate (must required)
    func connectionSocketPort() -> Int {
        return KIPCamSocketPort
    }
    func connectionHttpPort() -> Int {
        return KIPCamHttpPort
    }
```
#### Disconnection Task
###### OBJECTIVE-C
```objective-c
- (void)disconnect {
    NSString *SID = @"";/**Input your SID**/
    if ([[ORBM2MConnectionManager defaultManager] isAlreadyConnectedWithKey:SID]) {
        [[ORBM2MConnectionManager defaultManager] endConnectionWithSID:SID];

        // or disconnect all connections
        [[ORBM2MConnectionManager defaultManager] endAllConnections];
    }
}

```
###### SWIFT
```swift
    func disconnection() -> Void {
        let SID = ""/**Input your SID**/
        if ORBM2MConnectionManager.defaultManager().isAlreadyConnectedWithSID(SID) {
            ORBM2MConnectionManager.defaultManager().endConnectionWithSID(SID)
            
            // or disconnect all connections
            ORBM2MConnectionManager.defaultManager().endAllConnections()
        }
    }
```
#### Connection callback notification
###### OBJECTIVE-C
```objective-c
     //開始連線
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionEndConnect:) name:ORBM2MConnectionWillStartConnectNotification object:nil];
    //連線建立成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionDidConnect:) name:ORBM2MConnnectionDidConnectedNotification object:nil];
     //失去連線
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionEndConnect:) name:ORBM2MConnectionDidEndNotification object:nil];
```
###### SWIFT
```swift
    //開始連線
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mStartConnect), name: ORBM2MConnectionWillStartConnectNotification, object: nil)
    //連線建立成功
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mDidConnected), name: ORBM2MConnnectionDidConnectedNotification, object: nil)
    //失去連線    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mDidEnd), name: ORBM2MConnectionDidEndNotification, object: nil)
```

---
### ORBM2MSocketManager

`ORBM2MSocketManager` can manages socket based on `CFNetwork.frameowrk` 

#### Setup ORBM2MSocketManager
###### SWIFT
```swift
    var socketManager:ORBM2MSocketManager?

    func setup() -> Void {
         self.socketManager = ORBM2MSocketManager(mappedPort:connection!.socketMappedPort)
         self.socketManager!.mdelegate = self
    }
```

#### Send Command Request
###### SWIFT
```swift
    let mData:NSMutableData = NSMutableData.init()
    let command : String = ""/**Input your command**/
    let data:NSData = (command.dataUsingEncoding(NSASCIIStringEncoding))!
    mData.appendData(data)
    self.socketManager?.sendCommandData(mData, success: {
                        responseData in
                        print(responseData)
                    }, failure: {
                        error in
                        print(error)
                })
```
#### ORBM2MSocketManagerDelegate
###### OBJECTIVE-C
```objective-c
//MARK: - ORBSocketManagerDelegate
- (void)socket:(ORBM2MSocketManager*)socket didConnectStart:(NSString*)status {}
- (void)socket:(ORBM2MSocketManager*)socket didConnectFinished:(NSString*)status {}
- (void)socket:(ORBM2MSocketManager*)socket didConnectFail:(NSError*)error {}
- (void)socket:(ORBM2MSocketManager*)socket didReceiveError:(NSError*)error {}
- (void)socket:(ORBM2MSocketManager*)socket didResponseData:(NSData*)responseData {}
```
###### SWIFT
```swift
    //MARK: - ORBSocketManagerDelegate
    func socket(socket: ORBM2MSocketManager!, didConnectStart status: String!) {
        print("\(#function)")
    }
    
    func socket(socket: ORBM2MSocketManager!, didConnectFail error: NSError!) {
        print("\(#function)")
    }

    func socket(socket: ORBM2MSocketManager!, didReceiveError error: NSError!) {
        print("\(#function)")
    }
    
    func socket(socket: ORBM2MSocketManager!, didConnectFinished status: String!) {
        print("\(#function)")
    }

    func socket(socket: ORBM2MSocketManager!, didResponseData responseData: NSData!) {
        print("\(#function): \(responseData)")
    }
```


---


## Update history 

### 2016/6/21 ###
* Update ORBM2MKit.framework for connection renew method to closure.
* Update README.md

### 2016/6/20 ###
* Update ORBM2MKit.framework for connection renew delegate
* Update README.md

### 2016/6/17 ###
* Update ORBM2MKit.framework
* Update README.md

### 2016/6/3 ###
* Fix crash issue when receiving tunnel connection expected disconnect
* Add sample code about how to use callback functions with status of tunnel connections

## License

Copyright (c) 2015-2016 Orbweb Taiwan, Inc. (https://orbweb.me/)