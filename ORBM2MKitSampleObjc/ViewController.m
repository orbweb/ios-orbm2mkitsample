//
//  ViewController.m
//  ORBM2MKitSampleObjc
//
//  Created by Leo on 5/31/16.
//  Copyright © 2016 Orbweb Inc. All rights reserved.
//

#import "ViewController.h"
#import <OrbwebM2MKit/OrbwebM2MKit.h>

static NSString *sampleSid = @"D8S53Q7P9KREYC5E3DSU";

@interface ViewController () <ORBM2MConnectionManagerDelegate>

@property (nonatomic, strong) IBOutlet UITextField *textField;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) IBOutlet UILabel *port1Label;
@property (nonatomic, strong) IBOutlet UILabel *port2Label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _textField.text = sampleSid;
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionWillConnection:) name:ORBM2MConnectionWillStartConnectNotification object:nil];
    
    //未預期的斷線
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionEndConnect:) name:ORBM2MConnectionDidEndNotification object:nil];
    //連線建立成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionDidConnect:) name:ORBM2MConnnectionDidConnectedNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)connectButton:(id)sender {
    NSString *sid = _textField.text;
    if (sid) {
        [ORBM2MConnectionManager defaultManager].mDelegate = self;
        [[ORBM2MConnectionManager defaultManager]connectWithSID:sid success:^(ORBM2MConnection *connection, NSError* error){
            if (connection != nil) {
                self.statusLabel.text = @"success";
                NSLog(@"rtspMappedPort: %ld", (long)connection.rtspMappedPort);
                NSLog(@"socketMappedPort: %ld", (long)connection.socketMappedPort);
                NSLog(@"httpMappedPort: %ld", (long)connection.httpMappedPort);
                self.port1Label.text = [NSString stringWithFormat:@"%ld", (long)connection.rtspMappedPort];
                self.port2Label.text = [NSString stringWithFormat:@"%ld", (long)connection.socketMappedPort];
            }
            else{
                self.statusLabel.text = @"fail";
                NSLog(@"Error: %@", error.localizedDescription);
            }
        } renew:^(ORBM2MConnection *connection, NSError *error){
            if (connection != nil) {
                NSLog(@"renew rtspMappedPort: %ld", (long)connection.rtspMappedPort);
                NSLog(@"renew socketMappedPort: %ld", (long)connection.socketMappedPort);
                NSLog(@"renew httpMappedPort: %ld", (long)connection.httpMappedPort);
            }
            else{
                NSLog(@"Error: %@", error.localizedDescription);
            }
        }];
    }
}

- (IBAction)disconnect:(id)sender {
    NSString *sid = _textField.text;
    if (sid) {
        if ([[ORBM2MConnectionManager defaultManager] isAlreadyConnectedWithSID:sid]) {
            [[ORBM2MConnectionManager defaultManager] endAllConnections];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.port1Label.text = @"disconnect";
        });
    }
    
}

//MARK: - ORBM2MConnectionManagerDelegate
-(NSInteger)connectionSocketPort {
    return KIPCamSocketPort;
}

-(NSInteger)connectionHttpPort{
    return KIPCamHttpPort;
}

-(void)connectionManager:(ORBM2MConnectionManager *)manager didRenewConnection:(ORBM2MConnection *)connection {}

//MARK: - NSNotifications
- (void)receiveConnectionWillConnection:(NSNotification*)noti
{
    NSLog(@"tunnel connection will start connect");
}

- (void)receiveConnectionEndConnect:(NSNotification*)noti
{
    NSLog(@"tunnel connection expected disconnect");
}

- (void)receiveConnectionDidConnect:(NSNotification*)noti
{
    NSLog(@"tunnel connection did connect");
}

@end
