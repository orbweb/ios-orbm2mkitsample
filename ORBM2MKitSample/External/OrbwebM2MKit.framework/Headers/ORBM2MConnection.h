//
//  ORBM2MConnection.h
//  OrbwebM2MKit
//
//  Created by Orbweb Taiwan Inc on 5/8/15.
//  Copyright (c) 2015 Orbweb Taiwan Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

const static NSInteger kMaxMappingPort = 20001;
const static NSInteger kMinMappingPort = 10001;
const static NSInteger kIPCamRemotePort = 554;
const static NSInteger KIPCamSocketPort = 9001;
const static NSInteger KIPCamHttpPort = 81;

@interface ORBM2MConnection : NSObject

- (instancetype)initWithConnectionKey:(NSString *)key identifier:(NSString *)identifier;

@property (nonatomic, readonly) NSString *identifier;

@property (nonatomic, readonly) NSString *connectionSID;

@property (nonatomic, readonly) NSString *serverId;

@property (nonatomic, assign) NSInteger rtspMappedPort;

@property (nonatomic, assign) NSInteger socketMappedPort;

@property (nonatomic, assign) NSInteger httpMappedPort;

@property (nonatomic, strong) NSDictionary *mappedPortDict;


/*!
 @discussion mapping port
 @param remotePort  remote Port
 @param localPort Local port
 */
- (nullable NSNumber *)localMappedPortWithRemotePort:(NSInteger)remotePort startingLocalPort:(NSInteger)localPort error:(inout NSError **)outError;
/*!
 @discussion 停止Mapped port
 @param remotePort remote port
 */
- (void)unmapPortMappingWithRemotePort:(NSInteger)remotePort;

/*!
 @discussion 停止全部已配對的port
 */
- (void)unmapAllPortMappings;
@end

NS_ASSUME_NONNULL_END