//
//  ViewController.swift
//  M2MTestApp
//
//  Created by Orbweb Taiwan, Inc on 5/8/15.
//  Copyright (c) 2015 Orbweb Taiwan, Inc. All rights reserved.
//

import UIKit


class ViewController: UIViewController, ORBM2MConnectionManagerDelegate, ORBSocketManagerDelegate {
    let connectionManager = ORBM2MConnectionManager.defaultManager()
    
    let ipcamSID = "AD6880KNLWTTRAZAATAD"
    var socketManager:ORBM2MSocketManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mDidConnected), name: ORBM2MConnnectionDidConnectedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mStartConnect), name: ORBM2MConnectionWillStartConnectNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(m2mDidEnd), name: ORBM2MConnectionDidEndNotification, object: nil)
        
        ORBM2MConnectionManager.defaultManager().mDelegate = self
        
        ORBM2MConnectionManager.defaultManager().mDelegate = self
        ORBM2MConnectionManager.defaultManager().connectWithSID(ipcamSID, success: {
            connection, error -> Void in
            if error == nil {
                print("socketMappedPort: \(connection!.socketMappedPort)")
                print("rtspMappedPort: \(connection!.rtspMappedPort)")
                print("httpMappedPort: \(connection!.httpMappedPort)")
                
                if let cmd : String = "{\"CMD_ID\":\"DEVICE_RTSPINFO_QUERY_REQ\",\"ACCOUNT\":\"admin\",\"PWD\":\"admin\"}" {
                    self.socketManager = ORBM2MSocketManager(mappedPort:connection!.socketMappedPort)
                    self.socketManager!.mdelegate = self
                    
                    let mData:NSMutableData = NSMutableData.init()
                    var type:Int = 1
                    var len:Int = (cmd as NSString).length
                    
                    let typeData:NSData = NSData(bytes: &type, length: 4)
                    let lenData:NSData = NSData(bytes:&len, length: 4)
                    let data:NSData = (cmd.dataUsingEncoding(NSASCIIStringEncoding))!
                    mData.appendData(typeData)
                    mData.appendData(lenData)
                    mData.appendData(data)
                    self.socketManager!.sendCommandData(mData, success: {
                        responseData in
                        print("success: \(responseData)")
                        }, failure: {
                            error in
                    })
                }
                
            }
            else{
                print("ERROR[\(error!.code)]\nDomain: \(error!.domain)\nDescription:\(error!.localizedDescription)")
            }
            
            }, renew: {
                connection, error -> Void in
                if connection != nil {
                    print("renew socketMappedPort: \(connection!.socketMappedPort)")
                    print("renew rtspMappedPort: \(connection!.rtspMappedPort)")
                    print("renew httpMappedPort: \(connection!.httpMappedPort)")
                }
                else{
                    print("ERROR[\(error!.code)]\nDomain: \(error!.domain)\nDescription:\(error!.localizedDescription)")
                }
        })
        
    }
    
    //MARK: - ORBM2MConnectionNotification
    func m2mDidConnected(notification:NSNotification) -> Void {
        print("\(#function)")
    }
    
    func m2mStartConnect(notification:NSNotification) -> Void {
        print("\(#function)")
    }
    
    func m2mDidEnd (notification : NSNotification) {
        print("\(#function): \(notification.object)")
    }
    
    //MARK: - ORBM2MConnectionManagerDelegate
    func connectionSocketPort() -> Int {
        return KIPCamSocketPort
    }
    func connectionHttpPort() -> Int {
        return KIPCamHttpPort
    }
    
    //MARK: - ORBSocketDelegate
    func socket(socket: ORBM2MSocketManager!, didConnectStart status: String!) {
        print("\(#function)")
    }
    
    func socket(socket: ORBM2MSocketManager!, didConnectFail error: NSError!) {
        print("\(#function)")
    }

    func socket(socket: ORBM2MSocketManager!, didReceiveError error: NSError!) {
        print("\(#function)")
    }
    
    func socket(socket: ORBM2MSocketManager!, didConnectFinished status: String!) {
        print("\(#function)")
    }

    func socket(socket: ORBM2MSocketManager!, didResponseData data:NSData) {
        print("\(#function): \(data)")
    }
    
    func disconnection() -> Void {
        let SID = ""/**Input your SID**/
        if ORBM2MConnectionManager.defaultManager().isAlreadyConnectedWithSID(SID) {
            ORBM2MConnectionManager.defaultManager().endAllConnections()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

