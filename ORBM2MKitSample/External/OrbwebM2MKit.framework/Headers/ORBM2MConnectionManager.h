//
//  ORBM2MConnectionManager.h
//  OrbwebM2MKit
//
//  Created by Orbweb Taiwan, Inc on 5/7/15.
//  Copyright (c) 2015 Orbweb Taiwan, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class ORBM2MConnection;
@protocol ORBM2MConnectionManagerDelegate;

@interface ORBM2MConnectionManager : NSObject

@property (nonatomic, readonly) NSString *rendezvousServerDomainName;

/*!
 *  ORBM2MConnectionManagerDelegate must declare HttpPort(81) and SocketPort(9001)
 */
@property (nonatomic, weak) id<ORBM2MConnectionManagerDelegate> mDelegate;
/*!
 @discussion singleton (方便使用)
 */
+ (ORBM2MConnectionManager*)defaultManager;


/*! Project version number for OrbwebM2MKit.
 @
 */
+ (NSString*)OrbwebM2MKitVersionNumber;

/*!
 @discussion 自行建立ConnectionManager (如果使用Singleton物件，則不需要使用此函式。)
 @param 註冊合法的domainName
 */
- (instancetype)initWithRendezvousServerDomainName:(NSString *)domainName;

/*!
 @discussion 處理建立連線的函式
 @param SID
 Sample code to show how to connect
 
    [ORBM2MConnectionManager defaultManager].mDelegate = self;
    [[ORBM2MConnectionManager defaultManager]connectWithSID:sid success:^(ORBM2MConnection *connection, NSError* error){
        if (connection != nil) {
 
        }
        else{
 
        }
    } renew:^ (ORBM2MConnection *connection, NSError* error){
        if (connection != nil) {
 
        }
        else{
 
        }
    }];

 */
- (void)connectWithSID:(NSString*)SID
               success:(void(^)(ORBM2MConnection * __nullable connection, NSError * __nullable error))completeBlock
                 renew:(void(^)(ORBM2MConnection * __nullable connection, NSError * __nullable error))renewBlock;

/*!
 @param 
    SWIFT:
    let param =  ["socketPort":KIPCamSocketPort,
                  "rtspPort":kIPCamRemotePort,
                  "httpPort":KIPCamHttpPort]
    OBJ-C:
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @(KIPCamSocketPort),@"socketPort",
                                    @(kIPCamRemotePort),@"rtspPort",
                                    @(KIPCamHttpPort),@"httpPort", nil];
 */
- (void)connectWithSID:(NSString *)SID
            parameters:(NSDictionary*)param
               success:(void (^)(ORBM2MConnection * _Nullable, NSError * _Nullable))completeBlock
                 renew:(void (^)(ORBM2MConnection * _Nullable, NSError * _Nullable))renewBlock;

/*!
 @  Get connection if connection exists
 @param sid
 */
-(ORBM2MConnection*)getConnectionWithSID:(NSString*)SID;

/*!
 @discussion mapped other port number
 @param port other mappedPort if you need
 @param sid
 */
- (void)addPort:(NSInteger)port
        withSID:(NSString*)SID
        success:(void(^)(NSInteger mappedPort, NSError * __nullable error))completeBlock;

/*!
 @discussion 中斷連線
 @param key SID
 */
- (void)endConnectionWithSID:(NSString *)SID;
/*!
 @discussion 透過ServerID來中斷連線
 @param serverId 連立連線完成時回傳的ServerID
 */
- (void)endConnectionWithServerId:(NSString*)serverId;

/*!
 @discussion 中斷所有連線
 */
- (void)endAllConnections;

/*!
 @discussion 透過SID來清除此連線
 @param SID
 */
- (void)cleanUpTerminatedConnectionWithSID:(NSString *)SID;

/*!
 @discussion 判斷連線是否已經建立
 @param SID
 @return 連線是否存在
 */
- (BOOL)isAlreadyConnectedWithSID:(NSString *)SID;

@end

@protocol ORBM2MConnectionManagerDelegate <NSObject>
@required
- (NSInteger)connectionSocketPort;
- (NSInteger)connectionHttpPort;

@end

// Posted when the connection will start connect.
extern NSString * const ORBM2MConnectionWillStartConnectNotification;

// Posted when the connection did disconnect.
extern NSString * const ORBM2MConnectionDidEndNotification;

// Posted when the connection connected.
extern NSString * const ORBM2MConnnectionDidConnectedNotification;

// Posted when the connection notify something message.
extern NSString * const ORBM2MNotifyMessageNotification;

/* Used for Notification userinfo.
 *
 * notification.userinfo[ORBM2MConnectionServerID]
 *
 */
extern NSString * const ORBM2MConnectionServerID;
extern NSString * const ORBM2MConnectionClientID;

NS_ASSUME_NONNULL_END