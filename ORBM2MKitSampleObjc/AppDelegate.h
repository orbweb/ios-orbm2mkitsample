//
//  AppDelegate.h
//  ORBM2MKitSampleObjc
//
//  Created by Leo on 5/31/16.
//  Copyright © 2016 Orbweb Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

